# Incubateur des Territoires

Le site public de l'Incubateur des Territoires.


## Technologies mises en oeuvre

Le frontend est en Vue.js 2 avec [Nuxt](https://nuxtjs.org/fr/) comme framework.

Le contenu est géré grâce au CMS Headless [Directus](https://docs.directus.io/) (plus d'informations sur
les CMS Headless [ici](https://www.lafabriquedunet.fr/blog/headless-cms/)).

Le moteur de recherche intégré à l'application est fourni par [Meilisearch](https://docs.meilisearch.com/).

## Développement local

Voici les étapes à suivree pour lancer une instance locale du site de l'Incubateur des Territoires.

### Définir les variables d'environnement

Les variables d'environnement nécessaires sont les suivantes :

- API_URL et API_TOKEN pour communiquer avec le backend Directus.
- MEILISEARCH_HOST et MEILISEARCH_API_KEY pour le service de recherche Meilisearch.

### Lancer le service

*Note: Vous devez avoir installé npm sur votre machine.*

```
git clone https://gitlab.com/incubateur-territoires/incubateur/website-incubateur.git
cd website-incubateur
npm install
npm run dev
```

Le serveur de développement devrait être maintenant accessible sur `localhost:3000`.
