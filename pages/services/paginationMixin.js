import cloneDeep from 'lodash.clonedeep';

export const getInitialPagination = (limit = 8) => ({
  current: 1,
  limit,
  total: 0,
});

const paginationMixin = ({ initialPagination }) => ({
  data() {
    return {
      pagination: cloneDeep(initialPagination),
    };
  },

  methods: {
    setPagination({ current, total, limit }) {
      this.pagination = {
        limit: limit ?? this.pagination.limit,
        current: current ?? this.pagination.current,
        total: total ?? this.pagination.total,
      };
    },
  },
});

export default paginationMixin;
